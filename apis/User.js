import data from './Data'
const userApi = {
  getUser: function (username) {
    const userData = data.user.filter(user => user.username == username)
    return userData ? userData[0] : {}
  }
}